package com.Telerik;

import Adds.ToySounds;
import Adds.VehicleTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class VehicleTest {

    @Test
    public void getName_should_returnName_when_validNameIsGiven(){
        Vehicle toy = new Vehicle("Car", 2, ToySounds.Car, VehicleTypes.CAR);
        Assertions.assertEquals("Car", toy.getName());
    }

    @Test
    public void constructor_should_trow_when_nameIsNull(){
        Assertions.assertThrows(NullPointerException.class,
                () -> new Vehicle(null, 2, ToySounds.Car, VehicleTypes.CAR));
    }

    @Test
    public void constructor_should_trow_when_nameIsBelowMinLength(){
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Vehicle("O", 2, ToySounds.Car, VehicleTypes.CAR));
    }

    @Test
    public void constructor_should_trow_when_nameIsAboveMinLength(){
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new Vehicle("ThisIsOneVeryLongToyName", 2, ToySounds.Car, VehicleTypes.CAR));
    }

    @Test
    public void getAge_should_returnAge_when_AgeIsAdded(){
        Vehicle toy = new Vehicle("Car", 2, ToySounds.Car, VehicleTypes.CAR);
        Assertions.assertEquals(2, toy.getAge());
    }

    @Test
    public void getSound_should_returnNo_when_soundIsNull(){
        Vehicle toy = new Vehicle("Car", 2, null, VehicleTypes.CAR);
        Assertions.assertEquals(ToySounds.NO, toy.getSound());
    }

    @Test
    public void getSound_should_returnSound_when_soundIsValid(){
        Vehicle toy = new Vehicle("Car", 2, ToySounds.Car, VehicleTypes.CAR);
        Assertions.assertEquals(ToySounds.Car, toy.getSound());
    }

    @Test
    public void getVehicleType_should_returnType_when_typeIsValid(){
        Vehicle toy = new Vehicle("Car", 2, ToySounds.Car, VehicleTypes.CAR);
        Assertions.assertEquals(VehicleTypes.CAR, toy.getVehicleType());
    }
}