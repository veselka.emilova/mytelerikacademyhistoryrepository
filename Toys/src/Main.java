import Adds.ToySounds;
import Adds.VehicleTypes;
import com.Telerik.Toys;
import com.Telerik.Vehicle;

public class Main {

    public static void main(String[] args) {

        Toys car = new Vehicle("Car", 3, ToySounds.Car, VehicleTypes.CAR);
        System.out.println(car.toString());
    }
}
