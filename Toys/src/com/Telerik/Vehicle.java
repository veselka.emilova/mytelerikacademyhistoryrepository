package com.Telerik;

import Adds.ToySounds;
import Adds.VehicleTypes;

public class Vehicle extends Toys{

    private VehicleTypes vechicletype;

    public Vehicle(String name, int age, ToySounds sound, VehicleTypes vechicletype) {
        super(name, age, sound);
        this.vechicletype = vechicletype;
    }

    public VehicleTypes getVehicleType(){
        return vechicletype;
    }

    @Override
    public String additionalInfo() {
        return String.format("This toy is a: %s%n", getVehicleType());
    }
}
