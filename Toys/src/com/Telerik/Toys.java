package com.Telerik;

import Adds.ToySounds;

public class Toys{

    public static final int MIN_NAME_LENGTH = 2;
    public static final int MAX_NAME_LENGTH = 20;

    private String name;
    private int age;
    private ToySounds sound;


    public Toys(String name, int age, ToySounds sound){
        setName(name);
        setSound(sound);
        this.age = age;
    }

    public String getName(){
        return name;
    }

    public int getAge() {
        return age;
    }

    public ToySounds getSound() {
        return sound;
    }

    private void setName(String name) {

        if(name == null){
            throw new NullPointerException("Please enter name");
        }

        if(name.length() < MIN_NAME_LENGTH || name.length() >  MAX_NAME_LENGTH){
            throw new IllegalArgumentException("Invalid name");
        }
        this.name = name;
    }

    private void setSound(ToySounds sound){

        if(sound == null){
            sound = ToySounds.NO;
        }
        this.sound = sound;
    }

    public String toString(){
        return String.format("Toys name: %s%nRecommended age for %s is: %d%nThis toy makes %s sounds!%n" + additionalInfo(), getName(),getName(), getAge(), getSound());
    }

    public String additionalInfo() {
        return null;
    }
}
