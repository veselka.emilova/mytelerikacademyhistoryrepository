import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class MyArrayListTests {

    private MyList<Integer> list;

    @BeforeEach
    public void init() {
        //Arrange for the tests
        list = new MyArrayList<>();
    }

    @Test
    public void get_should_returnRightElement_when_indexIsValid() {
        //Arrange
        list.add(1);
        list.add(2);
        //Act
        Integer result = list.get(0);
        //Assert
        Assertions.assertEquals(1, result);
    }

    @Test
    public void get_should_throw_when_indexIsBelowZero() {
        //Arrange
        //Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(-1));
    }

    @Test
    public void get_should_throw_when_indexIsAboveTheBound() {
        //Arrange
        //Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(0));
    }

    @Test
    public void set_should_returnRightElement_when_elementIsChanged() {
        //Arrange
        list.add(1);
        list.add(2);
        //Act
        list.set(0, 0);
        Integer result = list.get(0);
        //Assert
        Assertions.assertEquals(0, result);
    }

    @Test
    public void getSize_should_returnInitialSizeOfArray_when_calledWithoutAddedElements() {
        //Arrange
        //Act
        //Assert
        Assertions.assertEquals(4, list.getSize());
    }

    @Test
    public void add_should_doubleItsLength_when_newAddedElementEqualsArrayLength() {
        //Arrange
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        //Act
        int result = list.getSize();
        //Assert
        Assertions.assertEquals(8, result);
    }

    @Test
    public void getLast_should_returnLastElement_when_lastIndexIsCalled(){
        list.add(1);
        list.add(2);
        list.add(3);

        Assertions.assertEquals(3, list.getLast());
    }

    @Test
    public void getFirst_should_returnFirstElement_when_indexZeroIsCalled(){
        list.add(1);
        list.add(2);

        Assertions.assertEquals(1, list.getFirst());
    }

    @Test
    public void findIndexOf_should_returnIndex_when_ValueIsCalled(){
        list.add(1);
        list.add(2);

        Assertions.assertEquals(0, list.findIndexOf(1));
    }

    @Test
    public void findIndexOf_should_returnNegativeOne_when_notPresentValueIsCalled(){
        list.add(1);
        list.add(2);

        Assertions.assertEquals(-1, list.findIndexOf(3));
    }

    @Test
    public void MyArrayListConstructor_should_addAllValues_when_gettingElementByIndex(){
        Integer[] e = {1, 2, 3};

        MyList<Integer> listWithLength = new MyArrayList<>(e);

        Assertions.assertEquals(3, listWithLength.get(2));
    }

    @Test
    public void Iterator_should_returnTrue_when_arrayHasNextPosition(){
        list.add(1);

        Assertions.assertTrue(list.iterator().hasNext());
    }

    @Test
    public void Iterator_should_returnFalse_when_arrayIsEmpty(){
        Assertions.assertFalse(list.iterator().hasNext());
    }

    @Test
    public void Iterator_should_returnCurrentPosition_when_nextIsUsed(){
        list.add(1);
        list.add(2);
        list.add(3);

        Iterator<Integer> iterator = list.iterator();
        int e =0;

        while(iterator.hasNext()){
            e = iterator.next();
        }

        Assertions.assertEquals(3, e);
    }
}

