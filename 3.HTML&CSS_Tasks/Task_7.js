'use strict';
document.getElementById("Profile_Page").style.display="none";
document.getElementById("Friends_Page").style.display="none";
const clickedHomePageButton =function(){
    document.getElementById("Profile_Page").style.display="none";
    document.getElementById("Friends_Page").style.display="none";
    document.getElementById("Home_Page").style.display="block";
}
const clickedProfilePageButton =function(){
    document.getElementById("Home_Page").style.display="none";
    document.getElementById("Friends_Page").style.display="none";
    document.getElementById("Profile_Page").style.display="block";
}
const clickedFriendsPageButton =function(){
    document.getElementById("Home_Page").style.display="none";
    document.getElementById("Profile_Page").style.display="none";
    document.getElementById("Friends_Page").style.display="block";
}