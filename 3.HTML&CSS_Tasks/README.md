# HTML Exercises

## Basics
* Create a webpage that prints your name to the screen.

* Create a webpage that prints the numbers 1 - 10 to the screen.

* Create a webpage and set its title to "This is a webpage".

* //Create a webpage that prints the message "When was this webpage created? Check page's title for the answer." to the screen, and set the title of the page to the current date.

## Texts

* Print your name in green.

* Print the numbers 1 - 10, each number being a different color. 

* Prints your name in a Tahoma font.

* Print a paragraph with 4 - 5 sentences. Each sentence should be a different font.

* Print a paragraph that is a description of a book, include the title of the book as well as its author. Names and titles should be underlined, adjectives should be italicized and bolded.

* Print your name to the screen with every letter being a different heading size.

## Links
* Create some links to various search engines (google, yahoo, altavista, lycos, etc).

* Create links to 3 different pages on 3 different websites that should all open in a new window.

* Create a page with a link at the top of it that when clicked will jump all the way to the bottom of the page.

* Create a page with a link at the bottom of it that when clicked will jump all the way to the top of the page.

## Images
* Display five different images. Skip two lines between each image. Each image should have a title.

* Display an image that has a border of size 2, a width of 200, and a height of 200.

* Display an image that when clicked will link to a search engine of your choice (should be opened in a new window).

* //Display an image that when clicked will link to itself and will display the image in the browser by itself.

*NOTE: Include the alt attribute in every < img > tag in the HTML image exercises.*


## Problem 1. Product description
*	Create a page that looks like this, using CSS styles:

![picture1](https://gitlab.com/TelerikAcademy/alpha-22-qa/raw/master/5.%20Frontend/1.%20HTML&CSS/images/robot-dog.png)

## Problem 2. Bar Jokes
*	Create a page that looks like this, using CSS styles:

![picture2](https://gitlab.com/TelerikAcademy/alpha-22-qa/raw/master/5.%20Frontend/1.%20HTML&CSS/images/bar-jokes.png)

## Problem 3. Studying Tips

*   Create a page taht looks like this, using CSS styles:

![picture3](https://gitlab.com/TelerikAcademy/alpha-22-qa/raw/master/5.%20Frontend/1.%20HTML&CSS/images/study-tips.png)

## Problem 4. Registration form
*	Create a Web form that looks like this sample:

![picture4](https://cloud.githubusercontent.com/assets/3619393/7003233/b2d1d68c-dc5f-11e4-8758-72d6af6d5cc0.png)

## Problem 5. Runners Home page
*	Write an HTML page like the following:

_Note: use headings, divs, paragraphs and ul_

![picture5](https://cloud.githubusercontent.com/assets/3619393/7002604/e4dadf2a-dc55-11e4-9743-76f4cf0c3f5b.png)


## Problem 6. Nested Lists
*	Write an HTML page like the following:

![picture6](https://cloud.githubusercontent.com/assets/3619393/7002607/ee1e65c0-dc55-11e4-966e-2eb85ea38c0d.png)

## Problem 7. Social site
*	Create an user profile Web page `profile.html`, friends page named `friends.html` and info page named `home.html`.
*	Link them to one another using `<a>` tag

![picture7](https://cloud.githubusercontent.com/assets/3619393/7002582/b53708fc-dc55-11e4-88f9-831a63a7a6c0.png)

![picture8](https://cloud.githubusercontent.com/assets/3619393/7002592/c14ee27c-dc55-11e4-85c5-42fefe9f9751.png)

![picture9](https://cloud.githubusercontent.com/assets/3619393/7002594/c9f92266-dc55-11e4-8396-f5ac08234561.png)

## Problem 8. Friendly formulas

*   Make a page that looks like this:

![picture10](https://gitlab.com/TelerikAcademy/alpha-22-qa/raw/master/5.%20Frontend/1.%20HTML&CSS/images/friendly-formulas.png)

## Problem 9. Text shadow

*   Make a page that looks like this:

![picture11](https://gitlab.com/TelerikAcademy/alpha-22-qa/raw/master/5.%20Frontend/1.%20HTML&CSS/images/text-shadow.png)



