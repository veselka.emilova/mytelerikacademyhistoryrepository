'use strict';

var slideIndex = 0;
slideShow();

function slideShow() {
  var i;
  var slides = document.getElementsByClassName("slides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" activedot", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " activedot";
  setTimeout(slideShow, 2000); // Change image every 2 seconds
}


document.getElementById("subText_1").style.display = "none";
document.getElementById("subText_2").style.display = "none";
document.getElementById("subText_3").style.display = "none";
document.getElementById("subText_4").style.display = "block";

document.getElementById("clickedIcon_1").style.display = "none";
document.getElementById("clickedIcon_2").style.display = "none";
document.getElementById("clickedIcon_3").style.display = "none";

const clickledsubText_1Button = function(){
    document.getElementById("subText_1").style.display="block";
    document.getElementById("subText_2").style.display="none";
    document.getElementById("subText_3").style.display="none";
    document.getElementById("subText_4").style.display ="none";

    document.getElementById("clickedIcon_1").style.display = "block";
    document.getElementById("clickedIcon_2").style.display = "none";
    document.getElementById("clickedIcon_3").style.display = "none";
    document.getElementById("firstIcon").style.display = "none";
    document.getElementById("secondIcon").style.display = "block";
    document.getElementById("thirdtIcon").style.display = "block";
}

const clickledsubText_2Button = function(){
    document.getElementById("subText_1").style.display="none";
    document.getElementById("subText_2").style.display="block";
    document.getElementById("subText_3").style.display="none";
    document.getElementById("subText_4").style.display ="none";

    document.getElementById("clickedIcon_2").style.display = "block";
    document.getElementById("clickedIcon_1").style.display = "none";
    document.getElementById("clickedIcon_3").style.display = "none";
    document.getElementById("firstIcon").style.display = "block";
    document.getElementById("secondIcon").style.display = "none";
    document.getElementById("thirdtIcon").style.display = "block";
}

const clickledsubText_3Button = function(){
    document.getElementById("subText_1").style.display="none";
    document.getElementById("subText_2").style.display="none";
    document.getElementById("subText_3").style.display="block";
    document.getElementById("subText_4").style.display ="none";

    document.getElementById("clickedIcon_3").style.display = "block";
    document.getElementById("clickedIcon_1").style.display = "none";
    document.getElementById("clickedIcon_2").style.display = "none";
    document.getElementById("firstIcon").style.display = "block";
    document.getElementById("secondIcon").style.display = "block";
    document.getElementById("thirdtIcon").style.display = "none";
}