import com.telerikacademy.set.MyHashSet;
import com.telerikacademy.set.MyHashSetImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SetTests {

    private MyHashSet<String> set;

    @BeforeEach
    public void before() {
        set = new MyHashSetImpl<>();
    }

    @Test
    public void add_should_returnTrue_when_addedElementIsCalled(){
        set.add("first");
        Assertions.assertTrue(set.contains("first"));
    }

    @Test
    public void add_should_returnFalse_when_absentElementIsCalled(){
        set.add("first");
        Assertions.assertFalse(set.contains("second"));
    }

    @Test
    public void add_should_notAddRepeatingElement_when_repeatingElementIsAdded(){
        set.add("first");
        set.add("first");
        Assertions.assertEquals(1, set.size());
    }

    @Test
    public void add_should_extendItsSize_when_sizeReachesItsTreshold12Elements(){
        set.add("first");
        set.add("second");
        set.add("third");
        set.add("forth");
        set.add("fifth");
        set.add("sixth");
        set.add("seventh");
        set.add("eight");
        set.add("ninth");
        set.add("tenth");
        set.add("eleventh");
        set.add("twelfth");

        Assertions.assertEquals(32, set.capacity());
    }

    @Test
    public void size_should_returnCorrectSize_when_sizeIsCalled(){
        set.add("first");
        set.add("second");
        set.add("third");

        Assertions.assertEquals(3, set.size());
    }

    @Test
    public void remove_should_throw_when_absentValueIsCalled(){
        set.add("first");
        set.add("second");
        set.add("third");

        Assertions.assertThrows(IllegalArgumentException.class,
                ()-> set.remove("forth"));
    }

    @Test
    public void remove_should_removesElement_when_validElementIsCalled(){
        set.add("first");
        set.add("second");
        set.add("third");

        set.remove("first");

        Assertions.assertFalse(set.contains("first"));
    }

}
